package ga.brunnofdc.uranking.events;

import ga.brunnofdc.uranking.data.DataManager;
import ga.brunnofdc.uranking.ranking.RankCacheRepository;
import ga.brunnofdc.uranking.domain.RankedPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinQuitEvent implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();

        DataManager.getDataSource().read(player, rank -> {
            RankedPlayer ranked = new RankedPlayer(player, rank);
            RankCacheRepository.storePlayer(player, ranked);
        });
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {

        Player player = e.getPlayer();
        RankedPlayer rankedPlayer = RankCacheRepository.getRankedPlayer(player);

        if(rankedPlayer != null) {
            DataManager.getDataSource().set(player, rankedPlayer.getRank().getId());
            RankCacheRepository.removePlayer(player);
        }

    }

}
