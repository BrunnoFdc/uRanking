package ga.brunnofdc.uranking.routines.support;

import ga.brunnofdc.uranking.integrations.github.GithubApiReleaseDetailsResponse;
import ga.brunnofdc.uranking.uRanking;
import ga.brunnofdc.uranking.utils.RestTemplate;
import ga.brunnofdc.uranking.domain.Version;

import java.util.logging.Logger;

import static ga.brunnofdc.uranking.uRanking.GITHUB_REPO_NAME;

public class UpdateCheckerRoutine implements Runnable {
    private final Logger logger = uRanking.getInstance().getLogger();

    private static final String LATEST_RELEASE_URL =
            "https://api.github.com/repos/" + GITHUB_REPO_NAME + "/releases/latest";

    public void run() {
        logger.info("Checking for updates...");

        final GithubApiReleaseDetailsResponse releaseDetails = RestTemplate
                .performGetRequest(LATEST_RELEASE_URL, GithubApiReleaseDetailsResponse.class);

        final Version latestVersion = new Version(releaseDetails.getTagName());
        final Version currentVersion = new Version(uRanking.getInstance().getDescription().getVersion());

        if(currentVersion.isOlderThan(latestVersion)) {
            logger.info("Update available! New version: " + latestVersion.getValue());
            logger.info("Click to download: https://bit.ly/uRankingUpdate");
        }
    }

}
