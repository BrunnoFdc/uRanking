package ga.brunnofdc.uranking.routines.rankup;

import ga.brunnofdc.uranking.api.PlayerRankupEvent;
import ga.brunnofdc.uranking.data.DataManager;
import ga.brunnofdc.uranking.domain.Rank;
import ga.brunnofdc.uranking.domain.RankedPlayer;
import ga.brunnofdc.uranking.economy.EconomicUnit;
import ga.brunnofdc.uranking.utils.RankUtils;
import ga.brunnofdc.uranking.uRanking;
import ga.brunnofdc.uranking.utils.StringList;
import ga.brunnofdc.uranking.utils.SystemDefs;
import ga.brunnofdc.uranking.domain.Message;
import ga.brunnofdc.uranking.domain.Property;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

import static ga.brunnofdc.uranking.utils.RankUtils.isMaxRank;
import static ga.brunnofdc.uranking.routines.rankup.RankupMessageParamsTranslatorRoutine.translate;
import static ga.brunnofdc.uranking.utils.Language.getMessage;
import static ga.brunnofdc.uranking.domain.Message.NO_MONEY;
import static ga.brunnofdc.uranking.domain.Message.RANKUP_ANNOUNCE;
import static ga.brunnofdc.uranking.domain.Message.RANKUP_MAX_ANNOUNCE;
import static ga.brunnofdc.uranking.domain.Message.RANKUP_MAX_PM;
import static ga.brunnofdc.uranking.domain.Message.RANKUP_PM;
import static ga.brunnofdc.uranking.domain.Property.BROADCAST_MAXRANK;
import static ga.brunnofdc.uranking.domain.Property.BROADCAST_RANKUP;
import static ga.brunnofdc.uranking.domain.Property.PM_MAXRANK;
import static ga.brunnofdc.uranking.domain.Property.PM_RANKUP;
import static org.bukkit.ChatColor.translateAlternateColorCodes;

public class RankupRoutine {
    private static final Map<Property, Message> MESSAGES_BY_PROPERTY = new HashMap<>();

    static {
        MESSAGES_BY_PROPERTY.put(PM_RANKUP, RANKUP_PM);
        MESSAGES_BY_PROPERTY.put(PM_MAXRANK, RANKUP_MAX_PM);
        MESSAGES_BY_PROPERTY.put(BROADCAST_RANKUP, RANKUP_ANNOUNCE);
        MESSAGES_BY_PROPERTY.put(BROADCAST_MAXRANK, RANKUP_MAX_ANNOUNCE);
    }

    /**
     * Run all the routines related to a rank up, and save the data.
     * @param rankedPlayer The player that is going to rank up.
     * @param targetRank The target rank
     * @param sendMessages If messages (broadcast and PM) should be sent.
     * @param takeMoney If the price of new rank should be taken of player's money.
     */
    public static void rankUp(final RankedPlayer rankedPlayer, final Rank targetRank,
                       final boolean sendMessages, final boolean takeMoney) {
        final EconomicUnit economicUnit = uRanking.getInstance().getEconomicUnit();
        final Player player = rankedPlayer.getPlayer();

        if(takeMoney) {
            if(economicUnit.has(player, targetRank.getPrice())) {
                economicUnit.withdraw(player, targetRank.getPrice());
            } else {
                final StringList message = translate(getMessage(NO_MONEY), rankedPlayer);

                player.sendMessage(message.toArray());
                return;
            }
        }

        if(sendMessages) {
            sendRankupMessages(rankedPlayer, targetRank);
        }

        rankedPlayer.setRank(targetRank);
        targetRank.getCommands().forEach(cmd -> {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd
                    .replace("@player", player.getName())
                    .replace("@rank", targetRank.getId())
                    .replace("@price", String.valueOf(targetRank.getPrice()))
            );
        });

        DataManager.getDataSource().set(player.getUniqueId(), targetRank.getId());
        Bukkit.getPluginManager().callEvent(new PlayerRankupEvent(rankedPlayer));
    }

    public static void rankUp(final RankedPlayer rankedPlayer, final boolean sendMessages, final boolean takeMoney) {
        final Rank nextRank = RankUtils.getNextRank(rankedPlayer.getRank());

        rankUp(rankedPlayer, nextRank, sendMessages, takeMoney);
    }

    private static void sendRankupMessages(final RankedPlayer rankedPlayer, final Rank targetRank) {
        final Player player = rankedPlayer.getPlayer();
        final Property pmProperty;
        final Property broadcastProperty;

        // TODO: Find a more elegant way to do this.
        if(isMaxRank(targetRank)) {
            pmProperty = PM_MAXRANK;
            broadcastProperty = BROADCAST_MAXRANK;
        } else {
            pmProperty = PM_RANKUP;
            broadcastProperty = BROADCAST_RANKUP;
        }

        if(SystemDefs.getProp(pmProperty)) {
            final StringList message = translate(getMessage(MESSAGES_BY_PROPERTY.get(pmProperty)), rankedPlayer);

            player.sendMessage(message.toArray());
        }

        if(SystemDefs.getProp(broadcastProperty)) {
            translate(getMessage(MESSAGES_BY_PROPERTY.get(broadcastProperty)), rankedPlayer)
                    .forEach(Bukkit::broadcastMessage);
        }

        if(SystemDefs.getProp(Property.SEND_TITLE_ENABLE)) {
            final ConfigurationSection titleSection = uRanking.getInstance()
                    .getConfig()
                    .getConfigurationSection("System-Behaviour.Send-Title");

            final String title = translateAlternateColorCodes('&', titleSection.getString("Title-Line"))
                    .replace("@rank", targetRank.getName())
                    .replace("@player", player.getName());
            final String subtitle = translateAlternateColorCodes('&', titleSection.getString("Subtitle-Line"))
                    .replace("@rank", targetRank.getName())
                    .replace("@player", player.getName());

            if(SystemDefs.getProp(Property.SEND_TITLE_GLOBAL)) {
                Bukkit.getOnlinePlayers().forEach(p -> p.sendTitle(title, subtitle));
            } else {
                player.sendTitle(title, subtitle);
            }
        }
    }
}
