package ga.brunnofdc.uranking.routines.rankup;

import ga.brunnofdc.uranking.domain.Rank;
import ga.brunnofdc.uranking.domain.RankedPlayer;
import ga.brunnofdc.uranking.economy.EconomicUnit;
import ga.brunnofdc.uranking.exceptions.MaxRankException;
import ga.brunnofdc.uranking.exceptions.MinRankException;
import ga.brunnofdc.uranking.utils.RankUtils;
import ga.brunnofdc.uranking.uRanking;
import ga.brunnofdc.uranking.utils.Language;
import ga.brunnofdc.uranking.utils.StringList;
import ga.brunnofdc.uranking.domain.SingleLineMessage;
import org.jetbrains.annotations.Nullable;

import static ga.brunnofdc.uranking.utils.StringList.collectToStringList;
import static ga.brunnofdc.uranking.domain.SingleLineMessage.NO_NEXT_RANK;
import static ga.brunnofdc.uranking.domain.SingleLineMessage.NO_OLD_RANK;
import static java.util.Optional.ofNullable;

public class RankupMessageParamsTranslatorRoutine {
    public static String translate(final String message, final RankedPlayer player) {
        Rank nextRank;
        Rank oldRank;

        try {
            nextRank = RankUtils.getNextRank(player.getRank());
        } catch (MaxRankException e) {
            nextRank = null;
        }

        try {
            oldRank = RankUtils.getOldRank(player.getRank());
        } catch (MinRankException e) {
            oldRank = null;
        }

        final EconomicUnit economicUnit = uRanking.getInstance().getEconomicUnit();
        final double playerBalance = economicUnit.getBalance(player.getPlayer());

        final String nextPrice = ofNullable(nextRank)
                .map(Rank::getPrice)
                .map(String::valueOf)
                .orElse(Language.getSingleLineMessage(NO_NEXT_RANK));

        final String nextRankName = getRankNameWithFallbackMessage(nextRank, NO_NEXT_RANK);

        final String oldRankName = getRankNameWithFallbackMessage(oldRank, NO_OLD_RANK);

        String remaining = String.valueOf(playerBalance);

        return message
                .replace("@price", nextPrice)
                .replace("@remaining", remaining)
                .replace("@balance", remaining)
                .replace("@player", player.getPlayer().getName())
                .replace("@rank", player.getRank().getName())
                .replace("@nextrank", nextRankName)
                .replace("@oldrank", oldRankName);
    }

    public static StringList translate(final StringList message, final RankedPlayer rankedPlayer) {
        return message.stream()
                .map(line -> translate(line, rankedPlayer))
                .collect(collectToStringList());
    }

    private static String getRankNameWithFallbackMessage(@Nullable final Rank rank,
                                                         final SingleLineMessage fallbackMessage) {
        return ofNullable(rank)
                .map(Rank::getName)
                .orElse(Language.getSingleLineMessage(fallbackMessage));
    }
}
