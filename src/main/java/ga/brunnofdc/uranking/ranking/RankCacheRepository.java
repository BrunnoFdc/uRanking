package ga.brunnofdc.uranking.ranking;

import ga.brunnofdc.uranking.domain.RankedPlayer;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static ga.brunnofdc.uranking.data.DataManager.getDataSource;

public class RankCacheRepository {
    private static final Map<UUID, RankedPlayer> rankedPlayers = new HashMap<>();

    public static void storePlayer(final Player player, final RankedPlayer rankedPlayer) {
        rankedPlayers.put(player.getUniqueId(), rankedPlayer);
    }

    public static void removePlayer(final Player player) {
        rankedPlayers.remove(player.getUniqueId());
    }

    public static void saveAllCachedData() {
        rankedPlayers.values().forEach(current -> {
            getDataSource().set(current.getPlayer().getUniqueId(), current.getRank().getId());
        });
    }

    public static void emptyCache() {
        saveAllCachedData();
        rankedPlayers.clear();
    }

    public static RankedPlayer getRankedPlayer(final Player player) {
        return rankedPlayers.get(player.getUniqueId());
    }

}
