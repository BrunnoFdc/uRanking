package ga.brunnofdc.uranking.integrations.github;


import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.io.Serializable;

@Data
public class GithubApiReleaseDetailsResponse implements Serializable {
    @SerializedName("tag_name")
    private String tagName;
}
