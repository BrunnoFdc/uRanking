package ga.brunnofdc.uranking.utils;

import com.google.gson.Gson;
import ga.brunnofdc.uranking.uRanking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * A utility class to make HTTP requests.
 * Inspired on Spring's RestTemplate.
 */
public class RestTemplate {
    public static <T> T performGetRequest(final String url, final Class<T> responseBodyType) {
        final String response = performGetRequest(url);

        return new Gson().fromJson(response, responseBodyType);
    }

    public static String performGetRequest(final String url) {
        try {
            URL urlInstance = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urlInstance.openConnection();

            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String inputLine;
                final StringBuilder result = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    result.append(inputLine);
                }
                in.close();

                return result.toString();
            }
        } catch (IOException error) {
            uRanking.getInstance().getLogger().severe("Error while performing GET request to " + url);
            error.printStackTrace();
        }

        return "";
    }
}
