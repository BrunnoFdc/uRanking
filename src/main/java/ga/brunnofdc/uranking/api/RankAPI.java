package ga.brunnofdc.uranking.api;

import ga.brunnofdc.uranking.domain.Rank;
import ga.brunnofdc.uranking.ranking.RankCacheRepository;
import ga.brunnofdc.uranking.utils.RankUtils;
import ga.brunnofdc.uranking.exceptions.MaxRankException;
import ga.brunnofdc.uranking.exceptions.MinRankException;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Deprecated
public class RankAPI {
    private Rank rank;

    public RankAPI(@NotNull final Player player) {
        this.rank = RankCacheRepository.getRankedPlayer(player).getRank();
    }

    private RankAPI(@NotNull final Rank rank) {
        this.rank = rank;
    }

    public int getPosition() {
        return rank.getPosition();
    }

    public String getDisplayname() {
        return rank.getName();
    }

    public String getID() {
        return rank.getId();
    }

    public String getTag() {
        return rank.getPrefix();
    }

    public double getPrice() {
        return rank.getPrice();
    }

    @Nullable
    public RankAPI getNextRank() {
        try {
            return new RankAPI(RankUtils.getNextRank(this.rank));
        } catch (MaxRankException ignored) {
            return null;
        }
    }

    @Nullable
    public RankAPI getOldRank() {
        try {
            return new RankAPI(RankUtils.getOldRank(this.rank));
        } catch (MinRankException ignored) {
            return null;
        }
    }

}
