package ga.brunnofdc.uranking.hooks;

import ga.brunnofdc.uranking.domain.RankedPlayer;
import ga.brunnofdc.uranking.exceptions.MaxRankException;
import ga.brunnofdc.uranking.utils.RankUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static com.google.common.collect.Maps.newHashMap;
import static ga.brunnofdc.uranking.domain.SingleLineMessage.NO_NEXT_RANK;
import static ga.brunnofdc.uranking.utils.Language.getSingleLineMessage;
import static java.lang.String.valueOf;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang.StringUtils.EMPTY;

public class PlaceholderRepository {
    private static final HashMap<String, Function<RankedPlayer, String>> MAPPERS_BY_PLACEHOLDER_ID = new HashMap<>();

    static {
        MAPPERS_BY_PLACEHOLDER_ID.put("rankname", rankedPlayer -> rankedPlayer.getRank().getName());
        MAPPERS_BY_PLACEHOLDER_ID.put("rankid", rankedPlayer -> rankedPlayer.getRank().getId());
        MAPPERS_BY_PLACEHOLDER_ID.put("rankprefix", rankedPlayer -> rankedPlayer.getRank().getPrefix());
        MAPPERS_BY_PLACEHOLDER_ID.put("rankpos", rankedPlayer -> valueOf(rankedPlayer.getRank().getPosition()));
        MAPPERS_BY_PLACEHOLDER_ID.put("rankprice", rankedPlayer -> valueOf(rankedPlayer.getRank().getPrice()));
        MAPPERS_BY_PLACEHOLDER_ID.put("nextrankname", rankedPlayer -> {
            try {
                return RankUtils.getNextRank(rankedPlayer.getRank()).getName();
            } catch (MaxRankException e) {
                return getSingleLineMessage(NO_NEXT_RANK);
            }
        });
        MAPPERS_BY_PLACEHOLDER_ID.put("nextrankid", rankedPlayer -> {
            try {
                return RankUtils.getNextRank(rankedPlayer.getRank()).getId();
            } catch (MaxRankException e) {
                return getSingleLineMessage(NO_NEXT_RANK);
            }
        });
        MAPPERS_BY_PLACEHOLDER_ID.put("nextrankprefix", rankedPlayer -> {
            try {
                return RankUtils.getNextRank(rankedPlayer.getRank()).getPrefix();
            } catch (MaxRankException e) {
                return getSingleLineMessage(NO_NEXT_RANK);
            }
        });
        MAPPERS_BY_PLACEHOLDER_ID.put("nextrankpos", rankedPlayer -> {
            try {
                return valueOf(RankUtils.getNextRank(rankedPlayer.getRank()).getPosition());
            } catch (MaxRankException e) {
                return getSingleLineMessage(NO_NEXT_RANK);
            }
        });
        MAPPERS_BY_PLACEHOLDER_ID.put("nextrankprice", rankedPlayer -> {
            try {
                return String.valueOf(RankUtils.getNextRank(rankedPlayer.getRank()).getPrice());
            } catch (MaxRankException e) {
                return getSingleLineMessage(NO_NEXT_RANK);
            }
        });
    }

    public static String getPlaceholder(@NotNull final String placeholderId,
                                        @Nullable final RankedPlayer rankedPlayer) {
        return ofNullable(rankedPlayer)
                .map(MAPPERS_BY_PLACEHOLDER_ID.get(placeholderId))
                .orElse(EMPTY);
    }

    public static boolean hasPlaceholder(final String placeholderId) {
        return MAPPERS_BY_PLACEHOLDER_ID.containsKey(placeholderId);
    }

    public static Map<String, Function<RankedPlayer, String>> getAllPlaceholders() {
        return newHashMap(MAPPERS_BY_PLACEHOLDER_ID);
    }
}
