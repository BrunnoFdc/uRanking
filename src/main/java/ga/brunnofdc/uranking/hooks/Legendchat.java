package ga.brunnofdc.uranking.hooks;

import br.com.devpaulo.legendchat.api.events.ChatMessageEvent;
import ga.brunnofdc.uranking.domain.Rank;
import ga.brunnofdc.uranking.ranking.RankCacheRepository;
import ga.brunnofdc.uranking.domain.RankedPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

@Deprecated
public class Legendchat implements Listener, Hook {

    @EventHandler
    public void onChat(ChatMessageEvent e) {
        final Player p = e.getSender();

        final RankedPlayer rankedPlayer = RankCacheRepository.getRankedPlayer(p);

        if(rankedPlayer != null) {
            final Rank rank = rankedPlayer.getRank();


            e.addTag("rank", rank.getPrefix());
        }
    }

    @Override
    public String getRelativePlugin() {
        return "Legendchat";
    }

    @Override
    public void setupHook(JavaPlugin plugin) {
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }
}
