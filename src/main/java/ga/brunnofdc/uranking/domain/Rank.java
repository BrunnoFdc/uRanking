package ga.brunnofdc.uranking.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.bukkit.ChatColor;

import java.util.List;

@Getter
@RequiredArgsConstructor
@ToString(exclude = "commands")
@EqualsAndHashCode(of = {"id"})
public class Rank implements Comparable<Rank> {
    private final String id;
    private final String name;
    private final String prefix;
    private final int position;
    private final double price;
    private final List<String> commands;

    public String getName() {
        return this.name + ChatColor.RESET;
    }

    public String getPrefix() {
        return this.prefix + ChatColor.RESET;
    }

    @Override
    public int compareTo(Rank rank) {
        return Integer.compare(this.getPosition(), rank.getPosition());
    }
}