package ga.brunnofdc.uranking.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;

@AllArgsConstructor
@Getter
@Setter
public class RankedPlayer {
    private final Player player;
    private Rank rank;


}
